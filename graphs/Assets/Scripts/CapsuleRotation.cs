using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    public float RotationSpeed; //velocidad de movimiento

    [SerializeField]

    private Vector3 RotationAxis; // posibles ejes de movimiento

    // Update is called once per frame
    void Update()
    {
        // Acotación de los valores de dirección al valor unitario [-1,1]
        RotationAxis = CapsuleMovement.ClampVector3(RotationAxis);

        // Desplazamiento del componente transform en base al tiempo
        transform.Rotate(RotationAxis * (RotationSpeed * Time.deltaTime));
    }

    /*
     * función auxiliar que permite acotar los valores de los componentes 
     * de un Vector3 entre -1 y 1.
    */
}
